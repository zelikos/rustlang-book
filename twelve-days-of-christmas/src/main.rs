fn main() {
    const DAY: [&str; 12] = ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth", "eleventh", "twelth"];
    const GIFTS: [&str; 12] = [
        "And a partridge in a pear tree.",
        "Two turtle doves,",
        "Three French hens,",
        "Four calling birds,",
        "Five golden rings,",
        "Six geese a-laying,",
        "Seven swans a-swimming,",
        "Eight maids a-milking,",
        "Nine ladies dancing,",
        "Ten lords a-leaping,",
        "Eleven pipers piping,",
        "Twelve drummers drumming,"
    ];
    for verse in 0..=11 {
        println!("On the {} day of Christmas, my true love sent to me", DAY[verse]);
        if verse == 0 {
            println!("A partridge in a pear tree.")
        } else {
            for gift in (0..=verse).rev() {
                println!("{}", GIFTS[gift]);
            }
        }
        println!();
    }
}
