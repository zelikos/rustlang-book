use rand::Rng;
use std::cmp::Ordering;
use std::io;

fn main() {
    println!("Guess the number!");

    let secret_number = rand::thread_rng().gen_range(1..=100);

    loop {
        let guess = match get_guess() {
            None => {
                println!("Thank you for playing.");
                println!("The number was: {}", secret_number);
                break;
            }
            Some(num) => num,
        };

        if guess == -1 {
            continue
        } else if guess != 0 {
            println!("You guessed {}", guess);

            match guess.cmp(&secret_number) {
                Ordering::Less => println!("Too small!"),
                Ordering::Greater => println!("Too big!"),
                Ordering::Equal => {
                    println!("A winner is you!");
                    break;
                }
            }
        } else {
            break;
        }
    }

}

fn get_guess() -> Option<i32> {
    println!("Please input your guess:");

    let mut guess = String::new();

    io::stdin()
        .read_line(&mut guess)
        .expect("Failed to read line");

    let guess = guess.trim();

    if guess == "quit" || guess == "exit" {
        None
    } else {
        let guess: i32 = match guess.parse() {
            Ok(num) => num,
            Err(_) => {
                println!("Guess must be a number.");
                -1
            }
        };
        Some(guess)
    }
}